import numpy as np
from itertools import product
from math import pi
import os,sys
import matplotlib.pyplot as plt

# Parameters
INPUT_FOLDER = "logs/"
INPUT_FILE_NAME = "3p_h1.csv"
OUTPUT_FILE = "3p_var_p_bitvar_.csv"
RESOLUTION = 0.01

optimum_win_probs = np.zeros(101)


num_lines = sum(1 for line in open(INPUT_FOLDER+INPUT_FILE_NAME))

class Player:
    # Define 4 deterministic strategies
    def get_answer(self, strategy, question):
        question = bool(question)
        if strategy == 0:
            return False
        if strategy == 1:
            return True
        if strategy == 2:
            return question
        if strategy == 3:
            return (not question)


class Game:
    #Class Constructor
    def __init__(self, number_of_players, resolution=None):
        self.number_of_players = number_of_players
        if resolution == None:
            self.resolution = 101
        else:
            self.resolution = resolution
        self.players = []
        for i in range(number_of_players):
            self.players.append(Player())
        #Every player can be asked 0=false or 1=true
        single_question = [False, True]

        #All possible combinations of question strings
        self.questions = list(product(single_question, repeat=number_of_players))
        #All possible combinations of player strategies
        self.strategies = list(product(range(4), repeat=number_of_players))
        
        self.variation = []
        self.gameResults= []

        #Ensure that dir exists
        #Path("graphs").mkdir(parents=True, exist_ok=True)

    def Play(self):
        for strategy in self.strategies:
            #[[questions],[answers],outcome]
            result = []
            for questions in self.questions:
                answers = []
                for i in range(len(self.players)):
                    answers.append(self.players[i].get_answer(strategy[i],questions[i]))

                outcome = self.WinCondition(answers, questions)
                result.append([list(questions), answers, outcome])
            self.gameResults.append(result)
        


    def WinCondition(self, answers, questions):
        x = 1
        for question in questions:
            x = x*question
        x = bool(x)
        y = None
        for i in range(len(answers)-1):
            if(y == None):
                y = answers[i]^answers[i+1]
            else:
                y = y^answers[i+1]
        if( x == y ):
            return True
        return False

    def AnalyzeOneStrategy(self,global_strategy):
        probability_spectrum = np.linspace(0,1,self.resolution)
        res = self.gameResults[global_strategy]
        win_array = []
        
        for p_1 in probability_spectrum:
            q_prob = self.GetQuestionProbabilityArray(p_1)
            win_rate = 0 
            for i in range(len(q_prob)):
                if(res[i][-1]):
                    win_rate = win_rate + q_prob[i]
            win_array.append(win_rate)

        self.variation.append(win_array)

    def GetQuestionProbabilityArray(self, p_1):
        p_0 = 1-p_1
        probability_combinations = list(product([p_0,p_1], repeat=self.number_of_players))
        string_probability = []
        for combination in probability_combinations:
            x = 1
            for a in combination:
                x = x*a
            string_probability.append(x)
        return string_probability

    def AnalyzeAllStrategies(self):
        for global_strategy in range(len(self.strategies)):
            self.AnalyzeOneStrategy(global_strategy)


    def PlotAll(self, fileName=None):
        #plt.figure(1)
        plt.title("Classical win probability of " + str(self.number_of_players) + " players"+
                "\n All deterministic strategies")
        plt.xlabel("Input question probability p_1")
        plt.ylabel("Win rate")
        for global_strategy in range(len(self.strategies)):
            plt.plot(np.linspace(0,1,self.resolution),self.variation[global_strategy])

        if(fileName==None):
            plt.show()
        else:
            fileName = "graphs/"+fileName
            plt.savefig(fileName)
        plt.clf()

    def PlotSupremum(self, fileName=None):
        supremum = np.zeros(self.resolution)
        for y in range(len(self.variation)):
            for x in range(len(supremum)):
                if self.variation[y][x] > supremum[x]:
                    supremum[x] = self.variation[y][x]
        #plt.figure(2)
        plt.title("Classical win probability of " + str(self.number_of_players) + " players" +
                "\n Sumpremum of all deterministic strategies")
        plt.xlabel("Input question probability p_1")
        plt.ylabel("Win rate")
        plt.plot(np.linspace(0,1,self.resolution), supremum)
        if(fileName==None):
            plt.show()
        else:
            fileName = "graphs/"+fileName
            plt.savefig(fileName)
        plt.clf()

    def GetSupremum(self):
        supremum = np.zeros(self.resolution)
        for y in range(len(self.variation)):
            for x in range(len(supremum)):
                if self.variation[y][x] > supremum[x]:
                    supremum[x] = self.variation[y][x]
        return supremum

    def PlotAllSeparate(self):
        dirName = str(self.number_of_players) + "_separate"
        #Path("graphs/"+dirName).mkdir(parents=True, exist_ok=True)
        for i in range(len(self.variation)):
            plt.title("Classical win probability: Separate Strategies of " + str(self.number_of_players)
                    + " players \n" + "Strategy: " + str(self.strategies[i]) )
            plt.xlabel("Input question probability p_1")
            plt.ylabel("Win rate")
            plt.plot(np.linspace(0,1,self.resolution),self.variation[i])

            plotName = str(self.number_of_players)+"-"+str(self.strategies[i]).replace(" ","")+".png"

            plt.savefig("graphs/"+dirName + "/" + plotName)
            plt.clf()

print("Playing Classical games")
g = Game(number_of_players=3)
g.Play()
g.AnalyzeAllStrategies()
print("Analyzing quantum games")
with open(INPUT_FOLDER+OUTPUT_FILE) as f:
    for line_num, line in enumerate(f):
        print("{:.3f}".format(line_num/num_lines))
        line = line.rstrip()
        line = line.split('|')
        win_probs = line[0].split(':')
        win_probs = np.array(win_probs).astype(np.float)

        for i in range(len(win_probs)):
            if( win_probs[i] > optimum_win_probs[i]):
                optimum_win_probs[i] = win_probs[i]
                #print(optimum_win_probs)
print(optimum_win_probs)
in_str_arr = np.linspace(0.0, 1.0, 101)

#plt.style.use("ggplot")

plt.title("Value")
plt.plot(in_str_arr, optimum_win_probs, label="Optimum non-local strategy")
plt.plot(in_str_arr, g.GetSupremum(), label="Classical strategy 1")
plt.legend(loc="lower right")
plt.xlabel("Input string probability")
plt.ylabel("Win rate")
axes = plt.gca()
axes.set_ylim([0.5,1.01])
axes.set_xlim([0,1])
import tikzplotlib
tikzplotlib.save("graphs/3p_var_in_str_prob.tex")
plt.savefig('graphs/3p_var_in_r_prob.png')    
