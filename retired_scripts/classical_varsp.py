import numpy as np
import itertools
from math import pi
import os,sys
import matplotlib.pyplot as plt

# Parameters
INPUT_FOLDER = "logs/"
INPUT_FILE_NAME = "2p_h1.csv"
OUTPUT_FILE = "2p_var_p.csv"
RESOLUTION = 0.01


wp = [1,1,1,0]
p11 = np.linspace(0.0,1.0,101)

