# Script fot analysis of simulation, where input string probabilities change


import numpy as np
import itertools
from math import pi
import os,sys

# Parameters
INPUT_FOLDER = "logs/"
INPUT_FILE_NAME = "3p_h1.csv"
OUTPUT_FILE = "3p_var_p_bitvar_.csv"
RESOLUTION = 0.01

PLAYERS = 3
LEVELS = 2

# Check if file exists
if not os.path.exists(INPUT_FOLDER):
    print("Log directory doesn't exist: "+ INPUT_FOLDER)
    sys.exit()

if not os.path.exists(INPUT_FOLDER+INPUT_FILE_NAME):
    print("Input file doesn't exist: " +INPUT_FOLDER+INPUT_FILE_NAME)
    sys.exit()

try:
    os.remove(INPUT_FOLDER+OUTPUT_FILE)
except OSError:
    pass

with open(INPUT_FOLDER+OUTPUT_FILE,'w'):pass

input_probs = [0.25, 0.25, 0.25, 0.25]


i_probabilities = np.linspace(0,1,num=1/RESOLUTION+1)


num_lines = sum(1 for line in open(INPUT_FOLDER+INPUT_FILE_NAME))
test_flag = 0
test_file_flag= 0

with open(INPUT_FOLDER+INPUT_FILE_NAME) as f:
    for line_num, line in enumerate(f):
        line = line.rstrip()
        
        line = line.split('|')
        #print(line)
        angles = line[3]#.replace('\r\n','')

        win_probs = line[2].replace('[','').replace(']','').split(',')
        win_probs = np.array(win_probs).astype(np.float)
        #print(win_probs)

        wp = []
       
        for p in i_probabilities:
            input_probabilities = []
            #CHANGE INPUT PROBABILITIES
            p_0 = p
            p_1 = 1-p

            input_probabilities = [p_0*p_0*p_0, p_0*p_0*p_1,p_0*p_1*p_0,p_0*p_1*p_1,p_1*p_0*p_0, p_1*p_0*p_1, p_1*p_1*p_0, p_1*p_1*p_1]
                    
            win_probability = 0
            for i in range(len(input_probs)):
                win_probability = win_probability + win_probs[i]*input_probabilities[i]
            wp.append(win_probability)
            if(p==0.5):
                if(win_probability > test_flag):
                    test_flag = win_probability


        row = []
        row.append(':'.join(map(str,wp)))
        row.append(':'.join(map(str,i_probabilities)))
        row.append(angles)

        if(win_probs[0] == 1):
            with open(INPUT_FOLDER + "test_file.csv", 'a') as of:
                of.write("|".join(row)+'\n')
                test_flag = 1
                
        
        #row = line

        row = '|'.join(row)

        with open(INPUT_FOLDER + OUTPUT_FILE, 'a') as of:
            of.write(row+'\n')
        print(str(line_num)+ " of " + str(num_lines))
        print("{:.3f}".format(line_num/num_lines))
        print(test_flag)
        print("Test file:" + str(test_file_flag))
