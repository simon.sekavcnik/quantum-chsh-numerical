import numpy as np
import matplotlib.pyplot as plt
import itertools



class Player:

    def get_answer(self, strategy, question):
        #print(strategy, question)
        if strategy == 0:
            return 0
        if strategy == 1:
            return 1
        if strategy == 2:
            return question
        if strategy == 3:
            return (question+1)%2
"""
        switch(strategy_num):
            Vjjjcase 0:
                return 0
            case 1:
                return 1
            case 2: 
                return question
            case 3:
                return (question+1)%2
"""
class Game:
    def __init__(self):
        self.p1 = Player()
        self.p2 = Player()
        self.p3 = Player()
        self.questions = list(itertools.product([0,1],[0,1],[0,1]))
        self.strategies = list(itertools.product(range(4),range(4),range(4)))
        print(self.strategies)
        print(self.questions)
        self.gameResults = []
        self.variation = []



    def Play(self):
        strategy = 0
        for strategy in self.strategies:
            result = []
            for q in self.questions:
                a = self.p1.get_answer(strategy[0], q[0])
                b = self.p2.get_answer(strategy[1], q[1])
                c = self.p3.get_answer(strategy[2], q[2])
                win = False
                if(bool(q[0]*q[1]*q[2])== ((bool(a)^bool(b)^bool(c)))):
                    win = True
                result.append([q[0],q[1],q[2],a,b,c,win])

            self.gameResults.append(result)
        #print(self.gameResults)

    def Analyze_one_strategy(self,strategy):
        #print(self.gameResults[strategy])
        res = self.gameResults[strategy]
        prob_spect = np.linspace(0.0,1.0,101)
        an_result = []
        for p_1 in prob_spect:
            p_0 = 1-p_1
            #q_prob = [p_0*p_0,p_0*p_1,p_1*p_0,p_1*p_1]
            q_prob = [p_0*p_0*p_0 ,p_0*p_0*p_1 ,p_0*p_1*p_0 ,
                        p_0*p_1*p_1 ,p_1*p_0*p_0 ,p_1*p_0*p_1 ,
                        p_1*p_1*p_0 ,p_1*p_1*p_1]

            win_rate = 0
            #print(q_prob)
            for y in range(len(q_prob)):
                if(res[y][-1]):
                    win_rate = win_rate + q_prob[y]

            if(p_0==0.5 and win_rate==0.875):
                print(self.strategies[strategy])


            an_result.append(win_rate)
        self.variation.append(an_result)
                
    def Analyze_all_strategies(self):
        for i in range(len(self.strategies)):
            self.Analyze_one_strategy(i)
        plt.title("Classical strategies")
        plt.xlabel("Input question probability")
        plt.ylabel("Win rate")
        for i in range(len(self.variation)):
            plt.plot(np.linspace(0,1,101),self.variation[i])
        plt.savefig("graphs/3p_classical_strategies.png")
        print(len(self.variation))


    def Supremum(self):
        supremum = np.zeros(101)

        for y in range(len(self.variation)):
            for x in range(len(supremum)):
                if self.variation[y][x] > supremum[x]:
                    supremum[x] = self.variation[y][x]
        plt.plot(np.linspace(0,1,101),supremum)
        plt.show()




g = Game()
g.Play()
g.Analyze_all_strategies()
g.Supremum()
