 
import numpy as np
import itertools
from math import pi
import os,sys
import matplotlib.pyplot as plt

# Parameters
INPUT_FOLDER = "logs/"
INPUT_FILE_NAME = "2p_h1.csv"
OUTPUT_FILE = "2p_var_p.csv"
RESOLUTION = 0.01

PLAYERS = 2
LEVELS = 2

optimum_win_probs = np.zeros(101)


num_lines = sum(1 for line in open(INPUT_FOLDER+INPUT_FILE_NAME))

with open(INPUT_FOLDER+OUTPUT_FILE) as f:
    for line_num, line in enumerate(f):
        print("{:.3f}".format(line_num/num_lines))
        line = line.rstrip()
        line = line.split('|')
        win_probs = line[0].split(':')
        win_probs = np.array(win_probs).astype(np.float)

        for i in range(len(win_probs)):
            if( win_probs[i] > optimum_win_probs[i]):
                optimum_win_probs[i] = win_probs[i]

print(optimum_win_probs)
in_str_arr = np.linspace(0.0, 1.0, 101)




#plt.style.use("ggplot")
plt.title("Win probability with changing probability of input string")
plt.plot(in_str_arr, optimum_win_probs, label="Optimum non-local strategy")
plt.plot(in_str_arr, np.linspace(1.0,0.0,101), label="Classical strategy 1")
plt.plot(in_str_arr, np.linspace(0.0,1.0,101), label="Classical strategy 2")
plt.legend(loc="lower right")
plt.xlabel("Input string probability")
plt.ylabel("Win rate")
axes = plt.gca()
axes.set_ylim([0.5,1.01])
axes.set_xlim([0,1])
import tikzplotlib
tikzplotlib.save("graphs/2p_var_in_bit_prob.tex")
plt.savefig('graphs/2p_var_in_bit_prob.png')    
