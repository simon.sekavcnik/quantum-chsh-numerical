import numpy as np
import itertools
from math import pi
import os

### PARAMETERS ###

NUMBER_OF_PLAYERS = 3
SYSTEM_LEVELS = 2
#ANGLES ARE OVERWRITTEN 
ANGLES = [[0,pi/4],[pi/6,-pi/3],[pi/12,-pi/6 ]]
LOG_FILE = "2p_h1.csv"
LOG_ANGLES = "2_players_angles.csv"
LOG_DIR = "logs/"

### PARAMETERS ###

max_win_rate = 0
resolution = 16


ket_0 = np.array([0,1])
ket_1 = np.array([1,0])
single_qubit_basis = [ket_0, ket_1]

def create_basis():
    """Creates basis, based on the NUMBER_OF_PLAYERS

    Returns
    -------
    list
        a list of all bases
    """

    players = []

    for i in range(NUMBER_OF_PLAYERS):
        a = []
        a.append(ket_0)
        a.append(ket_1)
        players.append(a)


    basis = []
    for i in list(itertools.product(*players)):
        base = 1
        for x in range(len(i)):
            base = np.kron(base,i[x])
        basis.append(base)
    return basis


def create_strings():
    """Creates list of all possible binary strings based on the NUMBER_OF_PLAYERS
    
    Returns
    -------
    list
        a list of all possible binary strings
    """

    players = []

    for i in range(NUMBER_OF_PLAYERS):
        a = []
        a.append(0)
        a.append(1)
        players.append(a)


    strings = []
    for i in list(itertools.product(*players)):
        strings.append(list(i))
    
    return strings

def create_entangled_qubits():
    """Creates GHZ state based on the NUMBER_OF_PLAYERS

    Returns
    -------
    np.array
        representing the GHZ state in matrix form
    """
    zeroes = ket_0
    ones = ket_1
    for i in range(NUMBER_OF_PLAYERS-1):
        zeroes = np.kron(zeroes,ket_0)
        ones = np.kron(ones,ket_1)

    entangled_system = np.sqrt(1/2)*(zeroes + ones) 
    return entangled_system

def create_real_unitary(angle):
    """Creates real unitary based on the provided angle
    U=[[ cos(angle), sin(angle)],[-sin(angle),cos(angle)]]

    Parameters
    ----------
    angle : float
        angle for unitary creation

    Returns
    -------
    np.matrix:
        matrix representing created unitary
    """
    return np.matrix([[np.cos(angle), np.sin(angle)], [-np.sin(angle), np.cos(angle)]])


def create_cmplx_unitary(angles):
    """Creates complex unitary based on provided angles

    Parameters
    ----------
    angles : list
        list with !three! elements of type float representing angles

    Returns
    -------
    np.matrix:
        matrix representing created unitary
    """
    x = (angle[0]+angle[2])/2
    y = (angle[0]-angle[2])/2
    b = angle[1]/2
    matrix = np.matrix([
        [(np.cos(-x)+1j*sin(-x))*cos(b), -(np.cos(-y)+1j*np.sin(-y))*sin(b)],
        [(np.cos(-y)+1j*sin(-y))*sin(b), (np.cos(x)+1j*sin(x))*cos(b)]
        ])
    return unitary
    

def combine_unitaries(unitaries):
    """Combines provided unitaries with kronecker product
    
    Parameters
    ----------
    unitaries: list
        a list of np.matrices representing unitary matrices

    Returns
    -------
    operation: np.matrix
        a matrix representing combined unitaries
    """
    operation = 1
    for unitary in unitaries:
        operation = np.kron(operation, unitary)
    return operation

def calc_probability(base,end_state):
    """ Calculates the probability of a measure (basis)

    Parameters
    ----------
    base: np.array
        Which base is being measured
    end_state: np.matrix
        State of whole system that is being measured

    Returns
    -------
    probability: float
        probability of this measure occuring
    """

    probability = abs(float(np.dot(([base]),np.transpose(end_state))[0]))**2
    return probability

def determine_win_probabilities(P, strings, string_probabilities):
    """Determins win probability of the game based on the measurement probabilty matrix

    Parameters
    ----------
    P: list (nested)
        nested list representing square measurement probability matrix
    strings: list
        list of all possible strings sent in the game
    string_probabilities: list
        list of probabilities of strings

    Returns
    -------
    win_rate: float
        float representing win rate of the game
    """
    '''
    win_rate = 0
    for x in range(len(P)):
        for y in range(len(P[x])):
            if win_condition(strings[x],strings[y]):
                win_rate = win_rate +  P[x][y]*string_probabilities[y]
    '''
    win_rates = []
    for x in range(len(P)):
        win_rate = 0
        for y in range(len(P[x])):
            if win_condition(strings[x],strings[y]):
                win_rate = win_rate +  P[x][y]#*string_probabilities[y]
        win_rates.append(win_rate)

    return win_rates
    




            
    return win_rate

def win_condition(inpt,outpt):
    """Determins wether the game was won based on the input and output

    Parameters
    ----------
    inpt: list
        list of integers representing sent string
    outpt: list
        list of integers representing received string

    Returns
    -------
    bool
        True if game was won, False otherwise
    """

    inpt_c = 1
    outpt_c = 1
    for i in inpt:
        inpt_c = inpt_c * i
    for i in outpt:
        outpt_c = outpt_c * -1 if i==1 else outpt_c
    outpt_c = 1 if outpt_c == -1 else 0
    if outpt_c == inpt_c:
        return True
    else:
        return False

def create_string_probabilities():
    """Creates normalized probability distribution for strings based on the
    NUMBER_OF_PLAYERS and SYSTEM_LEVELS
    At the moment it creates normal distribution

    Returns
    -------
    probabilities: list
        list of floats representing probability for each string
    """

    probabilities = []
    #Creates normal distribution
    for i in range(SYSTEM_LEVELS**NUMBER_OF_PLAYERS):
        probabilities.append(SYSTEM_LEVELS**(-NUMBER_OF_PLAYERS))
    return probabilities

#Function that calculates probabilitie for one game

def simulate_game(angles=None):
    """Simulates one game based on angles

    Parameters
    ----------
    angles: list
        nested list of floats representing angles (in radians) for each player
        Input is optional, but should normaly be provided

    Returns
    -------
    float
        Float representing how probable is the win with given parameters ( angles )

    TODO
    ----
    -> Should return the probability matrix for logging
    """

    ANGLES = angles
    entangled_system = create_entangled_qubits()
    bases = create_basis()
    strings = create_strings()
    string_probabilities = create_string_probabilities()
    P=[]
    for string in strings:
        unitaries = []
        for player in range(NUMBER_OF_PLAYERS):
            unitaries.append(create_real_unitary(ANGLES[player][string[player]]))
        operation = combine_unitaries(unitaries)
        end_state = np.dot(operation, entangled_system)

        #Get probability of each outcome
        p = []
        for base in bases:
            p.append(calc_probability(base, end_state))
        P.append(p)
        win_probabilities = determine_win_probabilities(P, strings, string_probabilities)
    return strings,win_probabilities
    



def log_results(angles, strings, win_probabilities):
    """Logs the results into csv file at LOG_DIR+LOG_FILE

    Parameters
    ----------
    angles: list
        list of angles that was used in this game
    win_rate: float
        calculated win rate (probability) for given angles
    """
    global max_win_rate

    if not os.path.exists(LOG_DIR):
        os.makedirs(LOG_DIR)

    if not os.path.exists(LOG_DIR + LOG_FILE):
        with open(LOG_DIR+LOG_FILE,'w'):pass

    log_angles = []
    for p in angles:
        log_angles.append(':'.join(str(v) for v in p))
    log_angles = ";".join(log_angles)

    avg_win_rate = np.average(win_probabilities)

    if(avg_win_rate>max_win_rate):
        max_win_rate=avg_win_rate

    print("{:.2f}".format(round(avg_win_rate, 2)),"|","{:.2f}".format(round(max_win_rate, 2)))
    
    row = [avg_win_rate, strings, win_probabilities, log_angles]
    row = "|".join(str(v) for v in row)

    with open(LOG_DIR + LOG_FILE,'a') as fd:
        fd.write(row+"\n")

def create_angle_file():
    """Creates csv file at LOG_DIR+LOG_ANGLE with all possible angle setups
    This is used to prevent from using too much RAM
    """
    print("Creating angle file")
    if not os.path.exists(LOG_DIR):
        os.makedirs(LOG_DIR)

    if os.path.exists(LOG_DIR + LOG_ANGLES):
        print(LOG_DIR + LOG_FILE)
        os.remove(LOG_DIR + LOG_ANGLES)

    global resolution

    possible_angles = pi*np.linspace(-1.0,1.0, num=resolution+1)
    #current_angles = np.zeros(NUMBER_OF_PLAYERS*SYSTEM_LEVELS)

    current_angles = []
    for i in range(NUMBER_OF_PLAYERS*SYSTEM_LEVELS):
        current_angles.append(0)

    angleCreationFlag = True
    with open(LOG_DIR + LOG_ANGLES,'a') as fd:
        while angleCreationFlag:


            print(current_angles)
            angles = []
            for p in range(NUMBER_OF_PLAYERS):
                a_p = []
                for r in range(SYSTEM_LEVELS):
                    a_p.append(possible_angles[current_angles[p*SYSTEM_LEVELS+r]])
                angles.append(a_p)

            log_angles = []
            for p in angles:
                log_angles.append(':'.join(str(v) for v in p))
            log_angles = ";".join(log_angles)
            
            fd.write(log_angles+"\n")

            testArr = []
            for i in range(len(current_angles)):
                testArr.append(resolution-1)
            if testArr == current_angles:
                angleCreationFlag = False
                break
            current_angles = custom_adder(current_angles,resolution)
            
            
    return True

def custom_adder(register, limit):
    """Full adder like function that increases register elements analogously
    *Used for iterating trough all of the angles

    Parameters
    ----------
    register: list
        list with elements of type int
    limit: int
        limit represents highes number which register element can reach
    """

    i = 0
    carryFlag = True
    limit = limit-1
    testArr = []
    for t in range(len(register)):
        testArr.append(limit)
    if testArr == register:
        return register
    while carryFlag:
        register[i] = register[i]+1
        if register[i] > limit:
            register[i] = 0
            carryFlag = True
            i = i+1
        else:
            carryFlag = False
    return register



"""
def simulate():
    pass
    
    Runs the simulation based on the provided paramters at the top

    global resolution

    create_angle_file()
    print("Simulating from angle file")
    combinations_num = resolution ** (NUMBER_OF_PLAYERS*SYSTEM_LEVELS)
    with open(LOG_DIR+LOG_ANGLES) as f:
        for i, line in enumerate(f):
            print("{:.2f}".format((i/combinations_num),2))
            line = line.rstrip()
            line = line.split(";")
            angles = []
            for a in line:
                a = a.split(":")
                angles.append(a)
            input_angles = np.array(angles).astype(np.float)
            strings, win_probabilities = simulate_game(input_angles)
            log_results(input_angles, strings, win_probabilities)
input_angles = ANGLES
strings, win_probabilities = simulate_game(input_angles)
print(input_angles, win_probabilities)

#simulate()
print("Simulation over")
"""
