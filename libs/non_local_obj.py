import numpy as np
from itertools import product


class Game():
    def __init__(self, number_of_players, questions=None, angles=None, angle_type=None,
            fidelity=1):
        self.number_of_players = number_of_players
        self._verify_questions(questions)
        self._verify_angles(angles, angle_type)
        self._create_basis()
        self._create_entangled_system(fidelity)
        self._create_unitary_array()
        self._calculate_win_probabilities()

    def _verify_questions(self, questions):
        if questions==None:
            self.question_set=list(product([0,1],repeat=self.number_of_players))
        else:
            for qs in questions:
                if len(qs) != self.number_of_players:
                    raise ValueError("Number of questions doesn't equate number_of_players")
            self.question_set = questions

    def _verify_angles(self, angles, angle_type):
        if angle_type == None or angle_type == 'unit':
            self.angle_type = 'unit'
        elif angle_type == 'pi':
            self.angle_type = 'pi'
        else:
            raise ValueError("angle_type can be 'pi', 'unit' or None")
        
        if len(angles) != self.number_of_players:
            raise ValueError("Angle dimensions don't match number_of_players")
        
        for a in angles:
            if len(a) == 2:
                if self.angle_type == 'pi':
                    for ang in a:
                        if abs(ang)>np.pi:
                            raise ValueError("Angle doesn't conform to constraints")
                if self.angle_type == 'unit':
                    for ang in a:
                        if abs(ang)>1:
                            raise ValueError("Angle doesn't conform to constraints")
            else:
                raise ValueError("Number of angles per player incorrect, should be 2")

        if self.angle_type == 'pi':
            self.angles = []
            for pair in angles:
                tp = []
                for angle in pair:
                    tp.append(angle/np.pi)
                self.angles.append(tp)
        elif self.angle_type == 'unit':
            self.angles = angles

    def _create_basis(self):
        self.basis = []
        for b in list(product([[0,1],[1,0]], repeat=self.number_of_players)):
            base = 1
            for ba in b:
                base = np.kron(base, ba)
            self.basis.append(list(base))
    
    def _create_entangled_system(self, fidelity):
        system = np.zeros(shape=(2**self.number_of_players, 2**self.number_of_players))
        if fidelity == 1:
            system[0][0] = system[2**self.number_of_players-1][0] = 1/2
            system[2**self.number_of_players-1][2**self.number_of_players-1] = 1/2
            system[0][2**self.number_of_players-1] = 1/2
        elif fidelity < 1 and fidelity >=0:
            for x in range(2**self.number_of_players):
                system[x][x] = (1-fidelity)/(2**self.number_of_players)
            
            e = fidelity/2 + (1-fidelity)/(2**self.number_of_players)
            system[0][0] = system[2**self.number_of_players-1][0] = e
            system[2**self.number_of_players-1][2**self.number_of_players-1] = e
            system[0][2**self.number_of_players-1] = e

        self.system = system

    def _create_unitary_array(self):
        self.unitaries = []
        for p_angles in self.angles:
            U = []
            for angle in p_angles:
                a = angle * np.pi
                U.append(np.matrix([[np.cos(a), np.sin(a)], [-np.sin(a), np.cos(a)]]))
            self.unitaries.append(U)

    def _combine_unitaries(self, unitaries):
        operation = 1
        for unitary in unitaries:
            operation = np.kron(operation, unitary)
        return operation

    def _calculate_win_probabilities(self):
        w_p = []
        for questions in self.question_set:
            U = []
            for i in range(self.number_of_players):
                U.append(self.unitaries[i][questions[i]])
            
            operation = self._combine_unitaries(U)

            state = np.dot(np.dot(operation,self.system),operation.conjugate().transpose())

            w_p.append(self._calculate_win_probability_for_question(state, questions))
        self.win_probabilities = w_p

    def _calculate_win_probability_for_question(self, state, questions):
        win_probability = 0
        for i in range(2**self.number_of_players):
            if(self._xor_int(i) == np.prod(questions)):
                win_probability = win_probability+state.item(i,i)

        return win_probability

    def _xor_int(self, number):
        in_arr = [int(d) for d in str(bin(number))[2:]]
        result = 0
        for x in in_arr:
            result = result + x
        result = result % 2
        return result

    def Grad(self):
        %TODO
        print("Calculating Gradient")
        pass        



g = Game(number_of_players=2, angles=[[0,1/4],[1/8,-1/8]], angle_type='unit', fidelity=1)
print("Win probability: %.2f %%" % (np.mean(g.win_probabilities)*100))
g.Grad()
