# Quantum CHSH Game 

Simulation script to reasearch win probabilities
of quantum CHSH game with any amount of players.

## Technologies
Project is created with:
* Python
* Numpy

## Setup
To run this project you will need virtual environment:
```
$ virtualenv .env
$ source .env/bin/activate
```

To deactivate virtual environment use the following command
```
(.env)$ deactivate
```

To install dependencies run the following:
```
(.env)$ pip install -r requirements.txt
```

To run the script:
```
(.env)$ python simulation.py
```

Before running you can configure settings in the beggining of the simulation.py

```
### PARAMETERS ###

NUMBER_OF_PLAYERS = 3
SYSTEM_LEVELS = 2
#ANGLES ARE OVERWRITTEN 
ANGLES = [[0,pi/4],[pi/8,-pi/8]]
LOG_FILE = "3_players_real_unitaries.csv"
LOG_ANGLES = "3_players_angles.csv"
LOG_DIR = "logs/"

### PARAMETERS ###
```
